
const express = require('express')
var bodyParser = require('body-parser')
var request = require('superagent')
var config = require('./config')

const app = express()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

var port = config.listen_port || 8112
app.post('/bitbucket-hook', (req, res, next) => {

    const repository = req.body.repository
    if (repository && repository.full_name) {
        console.log(repository.full_name)
        const endpoint = config.teamcity_scheme_authority || 'http://localhost:8111'
        const locator = `property:(name:url,value:${repository.full_name},matchType:contains)`
        const path = '/app/rest/vcs-root-instances/commitHookNotification'

        request.post(endpoint + path).query({locator: locator})
            .end((err, res) => {
                if (err) { console.error(err) }
                if (res) { console.log(res.text) }
            })
    }

    res.status(200).send()
})

app.listen(port, (err) => {
    if (err) { console.err(err) }
    console.info('listening on port ', port)
})
