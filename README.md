# Teamcity Bitbucket hook notifier

Listens for bitbucket hook requests and notifies teamcity to check VCS roots

Requires config.js with parameters
```
module.exports = {
    listen_port: 8112,
    teamcity_scheme_authority: 'http://user:pass@localhost:8111',
}
```